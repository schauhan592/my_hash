import React from "react";
import EncryptAndDecrypt from "./Component/EncryptAndDecrypt";
import Hashing from "./Component/Hashing";
import styled from "styled-components";

function App() {
  return (
    <Container>
      <Hashing />
      <EncryptAndDecrypt />
    </Container>
  );
}

export default App;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 10px;
`;
