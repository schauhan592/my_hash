import React, { useState } from "react";
import sha256 from "crypto-js/sha256";
import styled from "styled-components";

const Hashing: React.FC = () => {
  const [inputData, setInputData] = useState("");
  const [outputData, setOutputData] = useState("");
  const [toggle, setToggle] = useState(true);

  const handelChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setInputData(e.target.value);
    if (toggle) {
      const hex = sha256(e.target.value);
      setOutputData(hex.toString());
    }
  };

  const handelHash = () => {
    const hex = sha256(inputData);
    setOutputData(hex.toString());
  };

  return (
    <Container>
      <h1 style={{ textAlign: "center" }}>HASHING</h1>
      <InputBox value={inputData} onChange={handelChange} />
      <ButtonContainer>
        <button onClick={handelHash}>HASH</button>
        <input
          type="checkbox"
          defaultChecked={toggle}
          onClick={() => {
            setToggle(!toggle);
          }}
        />
      </ButtonContainer>
      <OutputBox>{outputData}</OutputBox>
    </Container>
  );
};

export default Hashing;

const Container = styled.div`
  border: 5px solid black;
  width: 80%;
  margin: auto;
  border-radius: 50px;
  padding: 10px;
  display: flex;
  flex-direction: column;
  row-gap: 10px;
`;

const InputBox = styled.textarea`
  width: 80%;
  height: 200px;
  margin: auto;
  vertical-align: top;
  resize: none;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  column-gap: 5px;
  align-items: center;
`;

const OutputBox = styled.div`
  width: 80%;
  height: 200px;
  margin: auto;
  border: 1px solid black;
`;
