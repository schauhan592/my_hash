import React, { useState } from "react";
import CryptoJS from "crypto-js";
import styled from "styled-components";

const EncryptAndDecrypt: React.FC = () => {
  const [encryptText, setEncryptText] = useState("");
  const [encryptedCode, setEncryptedCode] = useState("");
  const [encryptKey, setEncryptKey] = useState("");
  const [errorENC, setErrorENC] = useState("");

  const [decryptText, setDecryptText] = useState("");
  const [DecryptedCode, setDecryptedCode] = useState("");
  const [decryptKey, setDecryptKey] = useState("");
  const [errorDEC, setErrorDEC] = useState("");

  const handelEncrypt = () => {
    if (encryptKey && encryptText) {
      // Encrypt
      const ciphertext = CryptoJS.AES.encrypt(
        encryptText,
        encryptKey
      ).toString();
      setEncryptedCode(ciphertext);
    } else {
      setErrorENC("PLEASE PROVIDE ALL THE DATA !!!");
      setTimeout(() => {
        setErrorENC("");
      }, 2000);
    }
  };

  const hanelDecrypt = () => {
    if (decryptKey && decryptText) {
      // Decrypt
      const decryptData = CryptoJS.AES.decrypt(
        decryptText,
        decryptKey
      ).toString(CryptoJS.enc.Utf8);
      if (decryptData) {
        setDecryptedCode(decryptData);
      } else {
        setErrorDEC("Decrypte Key didn't match");
        setTimeout(() => {
          setErrorDEC("");
        }, 2000);
      }
    } else {
      setErrorDEC("PLEASE PROVIDE ALL THE DATA !!!");
      setTimeout(() => {
        setErrorDEC("");
      }, 2000);
    }
  };

  return (
    <Wrapper>
      <Container>
        <h1 style={{ textAlign: "center" }}>ENCRYPTION</h1>
        <div style={{ width: "80%", margin: "auto" }}>
          <h3>Encrypt Data :</h3>
          <InputBox
            value={encryptText}
            onChange={(e) => {
              setEncryptText(e.target.value);
            }}
          />
          <h3>Encrypt Key :</h3>
          <InputKey
            type="text"
            value={encryptKey}
            onChange={(e) => {
              setEncryptKey(e.target.value);
            }}
          />
          <div>{errorENC}</div>
          <ButtonContainer>
            <button onClick={handelEncrypt}>ENCRYPT</button>
          </ButtonContainer>
          <h3>Encrypted Data Code :</h3>
          <OutputBox>{encryptedCode}</OutputBox>
        </div>
      </Container>
      <Container>
        <h1 style={{ textAlign: "center" }}>DECRYPTION</h1>
        <div style={{ width: "80%", margin: "auto" }}>
          <h3>Decrypt Data Code :</h3>
          <InputBox
            value={decryptText}
            placeholder="Put Encrypted Code"
            onChange={(e) => {
              setDecryptText(e.target.value);
            }}
          />
          <h3>Decrypt Key :</h3>
          <InputKey
            type="text"
            value={decryptKey}
            onChange={(e) => {
              setDecryptKey(e.target.value);
            }}
          />
          <div>{errorDEC}</div>
          <ButtonContainer>
            <button onClick={hanelDecrypt}>DECRYPT</button>
          </ButtonContainer>
          <h3>Decrypted Data Code :</h3>
          <OutputBox>{DecryptedCode}</OutputBox>
        </div>
      </Container>
    </Wrapper>
  );
};

export default EncryptAndDecrypt;

const Wrapper = styled.div`
  border: 5px solid black;
  width: 80%;
  margin: auto;
  border-radius: 50px;
  padding: 10px;
  display: flex;
  flex-direction: column;
  row-gap: 10px;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const InputBox = styled.textarea`
  width: 100%;
  height: 100px;
  margin: auto;
  vertical-align: top;
  resize: none;
`;

const InputKey = styled.input`
  width: 100%;
`;

const OutputBox = styled.div`
  width: 100%;
  min-height: 50px;
  margin: auto;
  border: 1px solid black;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
`;
